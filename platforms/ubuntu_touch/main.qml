import QtQuick 2.9
import Qt.labs.settings 1.0

import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import Ubuntu.Content 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Window 2.2
import QtFeedback 5.0

import "components"

import GearBoy 1.0

QQC2.ApplicationWindow {
    id: applicationWindow
    
    property color gb_black: "#2A2A2A"
    property color gb_black_accent: "#3B3B3B"

    property color gb_gray: Qt.darker("#ABABAB", 1.1)
    property color gb_gray_accent: "#D0D0D0"

    property color gb_light_gray: "#D6D6D6"

    property color gb_white: "#E1E1E1"
    property color gb_white_accent: "#F7F7F7"

    property color gb_blue: "#45457e"
    property color gb_blue_accent: "#7462A2" //"#726C9E"
    
    property color gb_purple: "#B01561"
    property color gb_purple_accent: "#931946"
    
    property color gb_green: '#6c7a00'
    
    property color system_color: theme.palette.normal.background
    property color system_color_accent: theme.palette.normal.foreground
    
    property color ambiance: "white"
    property color ambiance_accent: "#F7F7F7"
    
    property color surudark: "#111111"
    property color surudark_accent: "#3B3B3B"
    
    height: units.gu(80)
    width: units.gu(45)
    minimumWidth: 300
    visible: false
    
    MainView {
        id: root
        
        readonly property string savesDir: "file://" + Files.savesDir

        property real thin_outline: units.gu(0.25)

        property bool muted: false
        property bool haptics: true

        property bool bold: true
        
        property int currentSlot: 1
        property bool showUI: false
        property bool fullscreen: false
        property bool fastSpeed: false
        
        property bool showUICriteria: (gameSettings.enableTouch || (!gameSettings.enableTouch && !gameSettings.hideOtherUI)  || root.showUI)
        
        property var currentKeymap: getMatchesInArray(gameSettings.externalInputs, gameSettings.selectedInput,"name")[0].keymap

        property var startKey: currentKeymap.start
        property var selectKey: currentKeymap.select
        property var upKey: currentKeymap.up
        property var downKey: currentKeymap.down
        property var leftKey: currentKeymap.left
        property var rightKey: currentKeymap.right
        property var aKey: currentKeymap.a
        property var bKey: currentKeymap.b
        property var pauseKey: currentKeymap.pause
        property var quickSaveKey: currentKeymap.quicksave
        property var quickLoadKey: currentKeymap.quickload
        property var fastKey: currentKeymap.fast
        property var muteKey: currentKeymap.mute
        property var prevSlotKey: currentKeymap.prevslot
        property var nextSlotKey: currentKeymap.nextslot
        
        property string loadedROM
        
        applicationName: "gearboy.bhdouglass"
        
        anchors.fill: parent

        Keys.onPressed: {
            if (emu.isRunning) {
                var key = event.key

                if (key == leftKey) {
                    emu.leftPressed()
                    event.accepted = true
                } else if (key == rightKey) {
                    emu.rightPressed()
                    event.accepted = true
                } else if (key == downKey) {
                    emu.downPressed()
                    event.accepted = true
                } else if (key == upKey) {
                    emu.upPressed()
                    event.accepted = true
                } else if (key == aKey) {
                    emu.aPressed()
                    event.accepted = true
                } else if (key == bKey) {
                    emu.bPressed()
                    event.accepted = true
                } else if (key == startKey) {
                    emu.startPressed()
                    event.accepted = true
                } else if (key == selectKey) {
                    emu.selectPressed()
                    event.accepted = true
                } else if (key == pauseKey) {
                    pauseSettings()
                    event.accepted = true
                } else if (key == fastKey) {
                    if (gameSettings.momentarySwitchSpeed) {
                        root.fastSpeed = true
                    } else {
                        toggleSpeed()
                    }
                    event.accepted = true
                } else if (key == muteKey) {
                    toggleSound(true)
                    event.accepted = true
                } else if (key == quickSaveKey) {
                    quickSave()
                    event.accepted = true
                } else if (key == quickLoadKey) {
                    quickLoad()
                    event.accepted = true
                } else if (key == prevSlotKey) {
                    prevSaveSlot()
                    event.accepted = true
                } else if (key == nextSlotKey) {
                    nextSaveSlot()
                    event.accepted = true
                }
                
                if (event.accepted) {
                    if (gameSettings.autoHideTouch && gameSettings.enableTouch) {
                        gameSettings.enableTouch = false
                    }
                    
                    if (gameSettings.hideOtherUI && root.showUI && (key != prevSlotKey && key != nextSlotKey)) {
                        root.showUI = false
                    }
                }
            }
        }

        Keys.onReleased: {
            if (emu.isRunning) {
                var key = event.key
                if (key == leftKey) {
                    emu.leftReleased()
                    event.accepted = true
                } else if (key == rightKey) {
                    emu.rightReleased()
                    event.accepted = true
                } else if (key == downKey) {
                    emu.downReleased()
                    event.accepted = true
                } else if (key == upKey) {
                    emu.upReleased()
                    event.accepted = true
                } else if (key == aKey) {
                    emu.aReleased()
                    event.accepted = true
                } else if (key == bKey) {
                    emu.bReleased()
                    event.accepted = true
                } else if (key == startKey) {
                    emu.startReleased()
                    event.accepted = true
                } else if (key == selectKey) {
                    emu.selectReleased()
                    event.accepted = true
                } else if (key == fastKey) {
                    if (gameSettings.momentarySwitchSpeed) {
                        root.fastSpeed = false
                    }
                    event.accepted = true
                } else if (key == prevSlotKey || key == nextSlotKey) {
                    if (gameSettings.hideOtherUI && !root.showUI) {
                        root.showUI = true
                    }
                    event.accepted = true
                }
            }
        }

        onMutedChanged: {
            emu.mute(muted)
        }
        
        onFastSpeedChanged: {
            if (fastSpeed) {
                emu.speed = gameSettings.speedMultiplier < 2 ? 2 : gameSettings.speedMultiplier
            } else {
                emu.speed = 1
            }
        }

        function click() {
            if (root.haptics) {
                pressEffect.start()
                Haptics.play()
            }
        }
        
        function load(url, name) {
            loadedROM = name
            gameListDialog.close()
            settingsPage.visible = false;
            
            var path = url.toString().replace("file://", "")
            if (path) {
                if (emu.loadRom(path)) {
                    var cheats = Files.readCheats(url.toString()).split('\n');

                    for (var i = 0; i < cheats.length; i++) {
                        if (cheats[i]) {
                            emu.setCheat(cheats[i]);
                        }
                    }

                    emu.play()
                } else {
                    help.text = i18n.tr("ROM failed to load")
                }
            }
        }
        
        function getMatchesInArray(arrayObj, value, attribute) {
            var matches =  arrayObj.filter(
              function(data){ return data[attribute] == value }
            );

            return matches
        }
        
        function toggleSound(notify) {
            gameSettings.sound = !gameSettings.sound
            
            if (notify) {
                var notifyText = !gameSettings.sound ? i18n.tr("Sound muted") : i18n.tr("Sound enabled")
                notifyTooltip.display(notifyText, "BOTTOM")
            }
        }
        
        function toggleSpeed() {
            fastSpeed = !fastSpeed
        }
        
        function loadLatestSlot() {
            savesFolderList.updateSaveData()
            
            loadDelay.start()
        }
        
        function quickSave() {
            emu.pause()
            emu.saveState(root.currentSlot);
            emu.play()
            notifyTooltip.display(i18n.tr("State saved in Slot" + " " +  root.currentSlot), "BOTTOM")
        }
        
        function quickLoad() {
            emu.pause()
            emu.loadState(root.currentSlot);
            emu.play()
            notifyTooltip.display(i18n.tr("State loaded from Slot" + " " +  root.currentSlot), "BOTTOM")
        }
        
        function pauseSettings() {
            emu.pause()
            if (emu.isRunning) {
                pauseDialog.open()
            } else {
                pagesDrawer.open()
            }
        }
        
        function prevSaveSlot() {
            var slot = root.currentSlot
            
            if (slot == 1) {
                slot = 5
            } else {
                slot = slot - 1
            }
            
            root.currentSlot = slot
        }
        
        function nextSaveSlot() {
            var slot = root.currentSlot
            
            if (slot == 5) {
                slot = 1
            } else {
                slot = slot + 1
            }
            
            root.currentSlot = slot
        }
        
        Component.onCompleted: {
            applicationWindow.visible = true
            applicationWindow.visibility = Qt.binding(function() { return root.fullscreen ? Window.FullScreen : Window.AutomaticVisibility; })
            
            emu.forceActiveFocus()
            
            btns.aPressed.connect(emu.aPressed)
            btns.aReleased.connect(emu.aReleased)

            btns.bPressed.connect(emu.bPressed)
            btns.bReleased.connect(emu.bReleased)

            select.pushed.connect(emu.selectPressed)
            select.unpushed.connect(emu.selectReleased)

            start.pushed.connect(emu.startPressed)
            start.unpushed.connect(emu.startReleased)

            dpad.upPressed.connect(emu.upPressed)
            dpad.upReleased.connect(emu.upReleased)

            dpad.downPressed.connect(emu.downPressed)
            dpad.downReleased.connect(emu.downReleased)

            dpad.leftPressed.connect(emu.leftPressed)
            dpad.leftReleased.connect(emu.leftReleased)

            dpad.rightPressed.connect(emu.rightPressed)
            dpad.rightReleased.connect(emu.rightReleased)
            
            dpad.upRightPressed.connect(emu.upPressed)
            dpad.upRightPressed.connect(emu.rightPressed)
            dpad.upRightReleased.connect(emu.upReleased)
            dpad.upRightReleased.connect(emu.rightReleased)
            
            dpad.upLeftPressed.connect(emu.upPressed)
            dpad.upLeftPressed.connect(emu.leftPressed)
            dpad.upLeftReleased.connect(emu.upReleased)
            dpad.upLeftReleased.connect(emu.leftReleased)
            
            dpad.downRightPressed.connect(emu.downPressed)
            dpad.downRightPressed.connect(emu.rightPressed)
            dpad.downRightReleased.connect(emu.downReleased)
            dpad.downRightReleased.connect(emu.rightReleased)
            
            dpad.downLeftPressed.connect(emu.downPressed)
            dpad.downLeftPressed.connect(emu.leftPressed)
            dpad.downLeftReleased.connect(emu.downReleased)
            dpad.downLeftReleased.connect(emu.leftReleased)
        }

        Component.onDestruction: {
            emu.shutdown()
        }
        
        Settings {
            id: gameSettings
            
            /* NOTE: Make sure to add new properties below all existing ones
             * otherwise, all properties below the new one 
             * will be reset to their default values */
            
            property bool vibrate: root.haptics
            property bool sound: !root.muted
            property string dmgPalette: 'original'
            
            property var externalInputs: [
                    {
                      "name": "Keyboard",
                      "type": "keyboard",
                      "keymap": {
                        "up": "87",
                        "down": "83",
                        "left": "65",
                        "right": "68",
                        "a": "75",
                        "b": "74",
                        "select": "16777248",
                        "start": "16777220",
                        "pause": "16777216",
                        "quicksave": "16777264",
                        "quickload": "16777265",
                        "fast": "32",
                        "mute": "77",
                        "prevslot": "16777234",
                        "nextslot": "16777236"
                      }
                    }
                  ]
                  
            property string selectedInput: "Keyboard"
            property string gbColor: "gb_white"
            property bool enableTouch: true
            property real touchOpacity: 1
            property bool hideOtherUI: false
            property bool autoHideTouch: false
            property bool fullscreen: false
            property int speedMultiplier: 2
            property bool momentarySwitchSpeed: false
            property bool autoLoadLatest: false

            onSoundChanged: root.muted = !sound
            onVibrateChanged: root.haptics = vibrate
            onFullscreenChanged: root.fullscreen = fullscreen
            onDmgPaletteChanged: emu.dmgPalette = dmgPalette
        }
        
        Timer {
            id: loadDelay
            
            interval: 2000 //2 seconds
            onTriggered: {
                var latestSlot = menuDrawer.latestSlot

                if (latestSlot > -1) {
                    root.currentSlot = latestSlot
                    root.quickLoad()
                }
            }
        }
    
        HapticsEffect {
            id: pressEffect
            
            attackIntensity: 0.0
            attackTime: 50
            intensity: 1.0
            duration: 10
            fadeTime: 50
            fadeIntensity: 0.0
        }
        
        NotifyTooltip{
            id: notifyTooltip
            
            oskPadding: keyboard.target.visible ? keyboard.target.keyboardRectangle.height : 0
        }


        Connections {
            target: ContentHub
            onImportRequested: {
                var path = transfer.items[0].url.toString().replace("file://", "")
                path = Files.moveRom(path);
                root.load(path);
            }
        }
        
        Connections {
            id: keyboard
            target: Qt.inputMethod
        }
        
        MouseArea {
            enabled: !gameSettings.enableTouch && gameSettings.hideOtherUI
            anchors.fill: parent
            z: -1
            onClicked: {
                root.showUI = !root.showUI
            }
        }

        GearBoyEmulator {
            id: emu
            color: applicationWindow[gameSettings.gbColor] ? applicationWindow[gameSettings.gbColor] : gb_white
            focus: true

            onIsRunningChanged: {
                if (isRunning) {
                    gameListPage.visible = false;
                    settingsPage.visible = false;
                    gameSettingsPage.visible = false;
                    forceActiveFocus()
                    
                    if (gameSettings.autoLoadLatest) {
                        root.loadLatestSlot()
                    }
                } 
            }
            
            onIsPausedChanged: {
                if (!isPaused) {
                    forceActiveFocus()
                }
            }
        }

        Rectangle {
            id: emuCover

            // TODO this covers the emulator when it's not running, figure out how to hide the emulator without covering it
            x: emu.rect.x
            y: parent.height - emu.rect.y - emu.rect.height
            width: emu.rect.width
            height: emu.rect.height
            visible: !emu.isRunning

            color: gb_green
            
            Label {
                id: help
                
                antialiasing: false
                visible: !emu.isRunning
                anchors.centerIn: parent
                text: i18n.tr("OPEN ROM…")
                textSize: Label.XLarge
                color: gb_black
                font.bold: true
            }

            MouseArea {
                id: loaderArea

                anchors.fill: parent

                onClicked: gameListDialog.open()
            }
        }

        Item {
            id: lefthand
            
            visible: gameSettings.enableTouch
            opacity: gameSettings.touchOpacity
            width: units.gu(18)
            height: units.gu(32)

            anchors {
                left: parent.left
                leftMargin: units.gu(1)
                bottom: parent.bottom
            }

            DirectionalPad {
                id: dpad
                x: 0
                y: 0
                width: parent.width
                height: width
                color: gb_black
                wingSize: units.gu(5.5)
                centerColor: gb_black_accent
                innerColor: gb_white_accent

                onLeftPressed: root.click()
                onRightPressed: root.click()
                onUpPressed: root.click()
                onDownPressed: root.click()
                
                onUpLeftPressed: root.click()
                onUpRightPressed: root.click()
                onDownLeftPressed: root.click()
                onDownRightPressed: root.click()
            }

            GBButton {
                id: select
                y: units.gu(22)

                anchors.right: parent.right

                buttonWidth: units.gu(9.5)
                buttonHeight: units.gu(2.5)
                touchPadding: units.gu(1)

                radius: height / 2

                text: i18n.tr("SELECT")
                textColor: gb_white_accent
                fontSize: "small"

                font {
                    bold: true
                }

                onPushed: root.click()
            }
        }

        Item {
            id: righthand
            
            visible: gameSettings.enableTouch
            opacity: gameSettings.touchOpacity
            anchors {
                right: parent.right
                rightMargin: units.gu(1)
                bottom: parent.bottom
            }
            width: units.gu(18)
            height: units.gu(32)

            GBButton {
                id: start
                y: select.y

                text: i18n.tr("START")
                textColor: select.textColor

                font: select.font
                fontSize: select.fontSize

                anchors.left: parent.left

                buttonWidth: units.gu(9.5)
                buttonHeight: units.gu(2.5)
                touchPadding: units.gu(1)

                radius: select.radius

                onPushed: root.click()
            }

            ButtonPad {
                y: 0
                x: units.gu(0.5)
                width: parent.width
                height: units.gu(18)
                id: btns

                onAPressed: root.click()
                onBPressed: root.click()

                font {
                    bold: root.bold
                }
            }
        }
        
        SaveFolderModel {
            id: savesFolderList
        }
        
        
        SaveLoadDrawer {
            id: menuDrawer
            
            property var modelData: [
                    { slot: 5, dateLabel: "" }
                    ,{ slot: 4, dateLabel: "" }
                    ,{ slot: 3, dateLabel: "" }
                    ,{ slot: 2, dateLabel: "" }
                    ,{ slot: 1, dateLabel: "" }                    
                ]
            
            interactive: saveLoadButton.visible
            
            model:  modelData
            
            onAboutToShow: {
                savesFolderList.refresh()
            }
        }
        
        CornerButton{
            id: saveLoadButton
            
            visible: emu.isRunning && root.showUICriteria
            corner: "left"
            iconName: "non-starred"
            subscriptText: root.currentSlot
            
            onClicked: {
                menuDrawer.open();
            }
        }
        
        CornerButton{
            id: pauseSettingsButton
            
            corner: "right"
            iconName: emu.isRunning ? 'media-playback-pause' : 'settings'
            visible: root.showUICriteria
            
            onClicked: {
                root.pauseSettings()
            }
        }
        

        PagesDrawer {
            id: pagesDrawer

            SettingsPage {
                id: settingsPage
                visible: false
                z: 100
            }
            
            InputPage {
                id: inputPage
                visible: false
                z: 100
            }
            
            ExternalInputPage {
                id: externalInputPage
                visible: false
                z: 100
            }
        }
        
        GameListDialog {
            id: gameListDialog
        
            GameListPage {
                id: gameListPage
                
                visible: false
                z: 100
            }

            GameSettingsPage {
                id: gameSettingsPage
                
                z: 100
                visible: false
            }
        }
        
        BottomEdgeControls {
            id: bottomEdgeControls
        }
        
        Icon{
            id: bottomEdgeHint
            
            name: "toolkit_bottom-edge-hint"
            width: units.gu(3)
            height: width
            visible: !bottomEdgeControls.visible && bottomEdgeControls.interactive &&  root.showUICriteria
            
            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
            
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    bottomEdgeControls.open()
                }
            }
        }
        
        QQC2.Dialog {
            id: pauseDialog
            
            property real maximumWidth: units.gu(50)
            property real preferredWidth: parent.width * 0.80
            
            width: preferredWidth > maximumWidth ? maximumWidth : preferredWidth
            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            parent: QQC2.ApplicationWindow.overlay
            modal: true
            closePolicy: QQC2.Popup.NoAutoClose
            padding: units.gu(4)
            
            background: Rectangle {
                    color: theme.palette.normal.background
                    radius: units.gu(1)
                }

            function closeDialog(play) {
                if (play) {
                    emu.play();
                }
                close()
            }
            
            onOpened: columnDialog.forceActiveFocus()
            
            Column{
                id: columnDialog
                
                spacing: units.gu(4)
                anchors.fill: parent
                
                Keys.onPressed: {
                    if (emu.isRunning) {
                        var key = event.key

                        if (key == root.pauseKey || key == root.bKey) {
                            pauseDialog.closeDialog(true)
                            event.accepted = true
                        }
                    }
                }
                
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(2)
                
                    RoundButton {
                        id: resumeButton
                        
                        buttonType: "positive"
                        iconName: 'media-playback-start'
                        width: units.gu(12)
                        height: width

                        onClicked: {
                            pauseDialog.closeDialog(true)
                        }
                    }
                    
                    RoundButton {
                        id: settingsButton

                        buttonType: "neutral"
                        iconName: 'settings'
                        width: units.gu(10)
                        height: width
                        anchors.verticalCenter: parent.verticalCenter

                        onClicked: {
                            pagesDrawer.open()
                            pauseDialog.closeDialog()
                        }
                    }
                }
            
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(2)
                    
                    RoundButton {
                        id: switchROMButton

                        buttonType: "neutral"
                        iconName: 'user-switch'
                        width: units.gu(8)
                        height: width

                        onClicked: {
                            gameListDialog.open()
                            pauseDialog.closeDialog()
                        }
                    }
                    
                    RoundButton {
                        id: restartButton

                        buttonType: "negative"
                        iconName: 'system-restart'
                        width: units.gu(8)
                        height: width

                        onClicked: {
                            emu.restart();
                            pauseDialog.closeDialog();
                        }
                    }
                    
                    RoundButton {
                        id: shutdownButton

                        buttonType: "negative"
                        iconName: 'system-shutdown'
                        width: units.gu(8)
                        height: width

                        onClicked: {
                            emu.shutdown();
                            pauseDialog.closeDialog();
                        }
                    }
                }
            }
        }
    }
}
