# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-07-19 15:39+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:24
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:104
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameSettingsPage.qml:35
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:63
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:24
msgid "Back"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:32
msgid "save"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:35
msgid "Input saved"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:35
msgid "Input added"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:39
msgid "Invalid or duplicate name. Please change"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:70
msgid "Up"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:71
msgid "Down"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:72
msgid "Left"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:73
msgid "Right"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:74
msgid "A"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:75
msgid "B"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:76
msgid "Select"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:77
msgid "Start"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:78
msgid "Pause"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:79
msgid "Quick Save"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:80
msgid "Quick Load"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:81
msgid "Previous Save Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:82
msgid "Next Save Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:83
msgid "Fast Forward"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:84
msgid "Mute"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:192
msgid "Enter input name"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:212
msgid "Keyboard"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:213
msgid "Gamepad"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:214
msgid "Others"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:250
msgid "Some gamepads may not be detected correctly due to system limitations"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/ExternalInputPage.qml:369
msgid "Press a key"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:99
msgid "ROMs"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:104
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:24
msgid "Close"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:112
msgid "Import ROM"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:146
msgid ""
"No ROMs available.\n"
"Tap the plus to add your own!"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:166
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:269
msgid "Delete"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameListPage.qml:177
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:282
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:19
msgid "Settings"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameSettingsPage.qml:43
msgid "Play"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/GameSettingsPage.qml:73
msgid "Cheat Codes"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:24
msgid "Edit input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:27
msgid "Add new input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:58
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:260
msgid "Input settings"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:71
msgid "Add"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:104
msgid "On-screen input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:110
msgid "Enable on-screen input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:125
msgid "Hide all on-screen buttons"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:126
msgid "Tap screen to toggle visibility of the buttons"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:137
msgid "Automatically disable on-screen buttons"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:138
msgid "When external input is detected"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:163
msgid "Opacity"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:193
msgid "External input"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:199
msgid "Fast forward only while key is pressed"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/InputPage.qml:200
msgid "Otherwise, it works like a toggle"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:239
msgid "ROM failed to load"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:256
msgid "Sound muted"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:256
msgid "Sound enabled"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:275
msgid "State saved in Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:282
msgid "State loaded from Slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:503
msgid "OPEN ROM…"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:561
msgid "SELECT"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/main.qml:590
msgid "START"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SaveFolderModel.qml:40
#, qt-format
msgid "a minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] ""
msgstr[1] ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SaveFolderModel.qml:43
#, qt-format
msgid "an hour ago"
msgid_plural "%1 hours ago"
msgstr[0] ""
msgstr[1] ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SaveFolderModel.qml:46
msgid "Just now"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SaveFolderModel.qml:50
msgid "Yesterday"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SaveFolderModel.qml:52
#, qt-format
msgid "%1 days ago"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:47
msgid "General settings"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:51
msgid "Sound"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:60
msgid "Vibration"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:69
msgid "Fullscreen"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:78
msgid "Emulator settings"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:84
msgid "Automatically load most recent save slot"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:96
msgid "Speed multiplier"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:137
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:162
msgid "GB White"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:139
msgid "GB Black"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:141
msgid "GB Gray"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:143
msgid "GB Blue"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:145
msgid "GB Purple"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:147
msgid "System Color"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:149
msgid "Ambiance"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:151
msgid "Suru Dark"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:153
msgid "GB color"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:195
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:230
msgid "Original"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:197
msgid "Grayscale"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:199
msgid "Brown"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:201
msgid "Pastel Mix"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:203
msgid "Blue"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:205
msgid "Green"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:207
msgid "Red"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:209
msgid "Orange"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:211
msgid "Dark Blue"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:213
msgid "Dark Green"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:215
msgid "Dark Brown"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:217
msgid "Yellow"
msgstr ""

#. TRANSLATORS: This is a color palette option
#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:219
msgid "Inverted"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:221
msgid "GB Palette"
msgstr ""

#: /home/brian/dev/ubuntu/apps/gearboy/platforms/ubuntu_touch/SettingsPage.qml:261
msgid "Touch"
msgstr ""
