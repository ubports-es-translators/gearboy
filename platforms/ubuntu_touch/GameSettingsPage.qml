import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls.Suru 2.2
import Qt.labs.folderlistmodel 2.1
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

import GearBoy 1.0

Page {
    id: gameSettings
    anchors.fill: parent

    property string game: ''
    property string fileUrl: ''
    property string romName: ''
    property string cheats: ''

    onFileUrlChanged: {
        cheats = Files.readCheats(fileUrl);
    }

    Rectangle {
        id: bkg
        anchors.fill: parent
        color: Suru.backgroundColor
    }

    header: PageHeader {
        title: game

        leadingActionBar.actions: [
            Action {
                iconName: 'back'
                text: i18n.tr("Back")
                onTriggered: gameSettings.visible = false
            }
        ]

        trailingActionBar.actions: [
            Action {
                iconName: 'media-playback-start'
                text: i18n.tr("Play")
                onTriggered: {
                    gameListPage.visible = false;
                    gameSettings.visible = false;
                    root.load(fileUrl, romName);
                }
            }
        ]

        flickable: flickable
    }

    Flickable {
        id: flickable

        contentHeight: column.height
        anchors.fill: parent

        ColumnLayout {
            id: column
            spacing: units.gu(1)

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: units.gu(1)
            }

            Label {
                text: i18n.tr("Cheat Codes")
            }

            TextArea {
                Layout.preferredHeight: units.gu(20)
                Layout.fillWidth: true
                text: cheats

                onTextChanged: Files.writeCheats(fileUrl, text);
            }
        }
    }
}
