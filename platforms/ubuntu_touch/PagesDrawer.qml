import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2

QQC2.Drawer {
    id: pagesDrawer
    
    readonly property real minimumWidth: units.gu(55)
    readonly property real preferredWidth: root.width * 0.25
    readonly property bool isFullWidth: width === root.width
    
    width: preferredWidth < minimumWidth ?  minimumWidth > root.width ? root.width + 1 : minimumWidth : preferredWidth
    height: root.height
    
    edge: Qt.RightEdge
    
    interactive: true
    
    onClosed: {
        interactive = true
        settingsPage.visible = false
        emu.play()
    }
    
    onOpened: {
        interactive = false
        emu.pause()
    }
    
    onAboutToShow: {
        settingsPage.visible = true
    }
    
    /* Workaround to allow closing the drawer with outside press
     * Disabling the interactive property disables both swipe closing and outside press 
     * We only need to disbale swipe closing */
    MouseArea {
        onClicked: pagesDrawer.close()

        anchors {
            right: parent.left
            left: parent.left
            leftMargin: -(root.width - parent.width)
            top: parent.top
            bottom: parent.bottom
        }
    }

}
