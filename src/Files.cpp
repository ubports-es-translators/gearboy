#include <QDebug>
#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include "Files.h"

Files *Files::m_files = nullptr;

Files::Files(QObject* parent) : QObject(parent) {
    QDir().mkpath(romDir());
    QDir().mkpath(settingsDir());
}

Files::~Files() {
    m_files = nullptr;
}

QString Files::romDir() {
    return QStandardPaths::writableLocation(QStandardPaths::DataLocation) + QStringLiteral("/roms/");
}

QString Files::savesDir() {
    return QStandardPaths::writableLocation(QStandardPaths::DataLocation) + QStringLiteral("/saves/");
}

QString Files::settingsDir() {
    return QStandardPaths::writableLocation(QStandardPaths::DataLocation) + QStringLiteral("/settings/");
}

QString Files::moveRom(const QString path) {
    QFileInfo fileInfo(path);
    QString newPath = romDir() + fileInfo.fileName();

    QFile f(path);
    bool ok = f.copy(newPath);
    qDebug() << "coping" << path << newPath << ok << f.error();

    if (ok) {
        removeRom(path);
        return newPath;
    }
    return path;
}

void Files::removeRom(const QString path) {
    QFile::remove(path);
}

QString Files::cheatPath(const QString gamePath) {
    QFileInfo fileInfo(gamePath);
    QString cheatFileName = fileInfo.completeBaseName() + QStringLiteral(".cheats");

    return settingsDir() + cheatFileName;
}

QString Files::readCheats(const QString gamePath) {
    QString cheatFileName = cheatPath(gamePath);

    QFile file(cheatFileName);
    if (file.exists()) {
        file.open(QFile::ReadOnly | QFile::Text);
        QTextStream stream(&file);

        return stream.readAll();
    }

    return QString();
}

void Files::writeCheats(const QString gamePath, const QString cheats) {
    QString cheatFileName = cheatPath(gamePath);

    QFile file(cheatFileName);
    file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    file.write(cheats.toUtf8());
    file.close();
}
